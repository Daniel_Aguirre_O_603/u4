#include<stdio.h>
#include<string.h>
#include<arpa/inet.h>   // ayuda a poner conecctar el servidor/ cliente por llamadas al sistema
//#include<sys/socket.h>

int open_socket();      // prototipos
void bind_to_port(int,int);   //entradas de conexion

int main(){
int PORT;
printf("enter the port to use:  \n");
scanf( "%d", &PORT);
// puesto a utilizar ... puede ser cualquiera superior a 1024
//char * advice[]= {"Ya funciona mi socket TCP\n",
//		"Hola  Daniel Aguirre Ordoñez\n",
//		"Daniel estas conectado\n",
//		"Soy el mensaje \n",};
char mensaje[1000];
printf("enter the message:   \n");
scanf( "%s", mensaje);
int listener = open_socket();  // espera conexion de clientes

if (listener == -1) {
printf("algo salio mal revisa tu funcion listener");
//return ;
}

 bind_to_port(listener, PORT);  // enlace al puerto

if (listen (listener, 10) == -1){   
printf("Este puerto esta ocupado o es incorrecto\n" );
//return  ;
}

printf ("linking to port ...\n");

while(1){
struct sockaddr_storage client;
unsigned int addres_size = sizeof(client);

printf ("waiting for client connection ...\n");

int connect = accept(listener, (struct sockaddr*) &client, &addres_size);
if(connect == -1){
printf("cannot connect secondary socket ...\n");
}


printf("serving connected client ...\n");

send (connect, mensaje, strlen(mensaje), 0);   // llamada al sistema envio de mensaje
//msg = NULL;
//close(connect);
}
return 0;
}


/*SOCKET*/
int open_socket(){
int s = socket(PF_INET,SOCK_STREAM, 0);  // dominio, flujo de datos enviados, protocolo
if (s == -1)
printf("Error al entrar al socket\n");
return s;
}



void bind_to_port(int socket, int port){
struct sockaddr_in name;
name.sin_family = PF_INET;
name.sin_port = (in_port_t)htons(port);
name.sin_addr.s_addr= htonl(INADDR_ANY);

int reuse = 1;
if(setsockopt(socket,SOL_SOCKET,SO_REUSEADDR, (char*)&reuse, sizeof(int)) == -1){
perror("socket cannot be reused ...\n");
}
int c = bind(socket, (struct sockaddr*) &name, sizeof(name));
if (c == -1){
perror ("cannot bind to given address ...\n");
}
}

